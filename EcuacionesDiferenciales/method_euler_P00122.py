import numpy as np

x0 = int(input("Ingrese el valor de X0: "))
y0 = int(input("Ingrese el valor de Y0: "))
xn = int(input("Ingrese el valor de XN: "))
n = int(input("Ingrese el número de iteraciones (N): "))
h =(xn-x0)/n
print("n     Xn      Yn")
print("%s     %s        %s"%(0,x0,y0))
for i in range(n):
    y0 = round(y0+h*(3*(x0)-2*(y0)), 3)
    x0 = round(x0+h, 3)
    print("%s     %s      %s" % (i+1, x0, y0))