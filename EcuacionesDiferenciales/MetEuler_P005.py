import numpy as np
import h5py
import sys

x0 = int(sys.argv[1])
y0 = int(sys.argv[2])
xn = int(sys.argv[3])
n = int(sys.argv[4])

matriz = np.zeros((n+1, 3))
h = (xn-x0)/n
matriz[0, 0] = 0
matriz[0, 1] = x0
matriz[0, 2] = y0

for i in range(n):
    x = i+1
    matriz[x, 0] = x
    matriz[x, 2] = round(matriz[x-1, 2]+h*(3*(matriz[x-1, 1])-2*(matriz[x-1, 2])), 3)
    matriz[x, 1] = round(matriz[x-1, 1]+h, 3)

with h5py.File('P005.h5', 'w') as f:
    dset = f.create_dataset("euler", data=matriz)
    f.close()