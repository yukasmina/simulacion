import numpy as np
import seaborn as sns
from numpy import random
import math

def exponencial(l):
    U=random.rand()
    return -1*(math.log(1/U)/l)

x=exponencial(0.5)
print(x)